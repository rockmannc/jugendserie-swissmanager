
import csv
import pprint
import time
from xlwt import Workbook, Pattern, Borders, easyxf

aks = [ 'U8', 'U9', 'U10', 'U12', 'U14', 'U16', 'Veteranen', 'U11', 'U7' ]

# borders = [Borders(),Borders()]
# for b in borders:
#   b.top = Borders.THIN
#   b.bottom = Borders.THIN

# patterns = [Pattern(),Pattern()]
# for p in patterns:
#   p.pattern = Pattern.SOLID_PATTERN

# patterns[0].pattern_back_colour = 0xFF
# patterns[1].pattern_back_colour = 0xC0

style = [
          easyxf('borders: top thin, bottom thin; pattern: pattern solid, fore_colour white; alignment: vertical center;'),
          easyxf('borders: top thin, bottom thin; pattern: pattern solid, fore_colour gray25; alignment: vertical center;'),
          easyxf('borders: top thin, bottom thin; pattern: pattern solid, fore_colour white; alignment: vertical center, horizontal center;'),
          easyxf('borders: top thin, bottom thin; pattern: pattern solid, fore_colour gray25; alignment: vertical center, horizontal center;')
        ]



style_title = easyxf('borders: bottom thick; font: bold true; alignment: horizontal center;')

sheets = []
player = []
book = Workbook(encoding='utf-8')
for ak in aks:
  sheets.append(book.add_sheet('Anmeldungen '+ak))
  player.append([])

line = 1
with open("import.lst", "rb") as f:
    reader = csv.reader(f.read().split("\n"),delimiter=';')
    for row in reader:
       if len(row) > 9:
           print row[0] + " / " + row[11]
           if row[4] == ' ':
             row[4] = '0'
           if row[9] == ' ':
             row[9] = '0'
           row[0] = row[0].decode('latin-1')
           row[1] = row[1].decode('latin-1')
           player[int(row[11])-1].append(row)
           line = line + 1

i = 0
titels = [ '', 'Anwesend', 'Name', 'Verein/Schule', 'JWZ', 'Geschl.', 'Quittung', 'erstellt' ]
for ak in aks:
  sheet=sheets[i]
  sheet.fit_num_pages = 1
  sheet.fit_width_to_pages = 1
  sheet.fit_height_to_pages = 0
  sheet.header_str = 'Anmeldungen '+ak
  sheet.footer_str = 'Stand: '+time.strftime("%d.%m.%Y %H:%M")
  row = sheet.row(0)
  nw = []
  for j in range(len(titels)):
    row.write(j,titels[j],style_title)
    nw.append(int((1 + len(titels[j])) * 256))
  r = 1
  for p in sorted(player[i], key=lambda plr: plr[0]):
    row = sheet.row(r)
    row.write(0,int(r),style[r%2])
    row.write(1,'O',style[r%2+2])
    row.write(2,p[0],style[r%2])
    row.write(3,p[1],style[r%2])
    row.write(4,int(p[4]),style[r%2])
    row.write(5,p[10],style[r%2+2])
    row.write(6,'O',style[r%2+2])
    row.write(7,'O',style[r%2+2])
    row.height = 2*256
    row.height_mismatch = True
    mnw = int((1 + len(str(r))) * 256)
    if nw[0] < mnw:
      nw[0] = mnw
    mnw = int((1 + len(p[0])) * 256)
    if nw[2] < mnw:
      nw[2] = mnw
    mnw = int((1 + len(p[1])) * 256)
    if nw[3] < mnw:
      nw[3] = mnw
    mnw = int((1 + len(p[4])) * 256)
    if nw[4] < mnw:
      nw[4] = mnw
    mnw = int((1 + len(p[10])) * 256)
    if nw[5] < mnw:
      nw[5] = mnw
    r = r + 1
  for c in range(len(nw)):
    sheet.col(c).width = nw[c] + 10
  i = i + 1

book.save('anmeldungen.xls')
