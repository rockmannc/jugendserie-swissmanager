
import csv
import pprint

aks = [ 'U8', 'U9', 'U10', 'U12', 'U14', 'U16', 'Veteranen', 'U11', 'U7' ]

line = 1
with open("import.lst", "rb") as f:
  with open("player.txt", "wb") as o:
    of = []
    wr = []
    i = 0
    for ak in aks:
      of.append(open("player-"+ak+".txt", "wb"))
      wr.append(csv.writer(of[i], delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL))
      wr[i].writerow([ 'Nr','Name','Titel','Identnr','EloNat','EloInt','Geburt','Fed','Sex','Typ','Gr','KlubNr','Klub','FideIdent','Quelle' ])
      i = i + 1
    reader = csv.reader(f.read().split("\n"),delimiter=';')
    writer = csv.writer(o, delimiter=';', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    writer.writerow([ 'Nr','Name','Titel','Identnr','EloNat','EloInt','Geburt','Fed','Sex','Typ','Gr','KlubNr','Klub','FideIdent','Quelle' ])
    for row in reader:
       if len(row) > 9:
           print row[0]
           if row[4] == ' ':
             row[4] = '0'
           if row[9] == ' ':
             row[9] = '0'
           writer.writerow([ line, row[0], '', int(row[9]), int(row[4]), int(row[4]), '01.01.'+row[6].split('.')[2], '', row[10], '', aks[int(row[11])-1], '', row[1], '', '' ])
           wr[int(row[11])-1].writerow([ line, row[0], '', int(row[9]), int(row[4]), int(row[4]), '01.01.'+row[6].split('.')[2], '', row[10], '', aks[int(row[11])-1], '', row[1], '', '' ])
           line = line + 1
