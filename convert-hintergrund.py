import csv
from datetime import date
from xlwt import Workbook, XFStyle

book = Workbook(encoding='utf-8')
sheet1 = book.add_sheet('Sheet 1')

sheet1.write(0,0,'ID_No')
sheet1.write(0,1,'Name')
sheet1.write(0,2,'Sex')
sheet1.write(0,3,'Fed')
sheet1.write(0,4,'Clubnumber')
sheet1.write(0,5,'ClubName')
sheet1.write(0,6,'Birthday')
sheet1.write(0,7,'Rtg_Nat')
sheet1.write(0,8,'Fide_No')
sheet1.write(0,9,'Rtg_Int')
sheet1.write(0,10,'Title')
sheet1.write(0,11,'K')

style = XFStyle()
style.num_format_str='DD.MM.YYYY'

line = 1
with open("hintergrund.txt", "rb") as f: 
    reader = csv.reader(f.read().split("\n"),delimiter=';')
    for row in reader:
       if len(row) > 9:
           print '\t'.join(row)
           print row[9]
           sheet1.write(line,0,int(row[9]))
           sheet1.write(line,1,row[0].decode('cp852'))
           sheet1.write(line,2,row[10])
           sheet1.write(line,5,row[1].decode('cp852'))
           bdate = int(row[6].split('.')[2])
           if bdate < 100:
             bdate = 2000+bdate
           sheet1.write(line,6,date(bdate,1,1),style)
           sheet1.write(line,7,int(row[4]))
           sheet1.write(line,9,int(row[4]))
           line = line + 1
        
book.save('hintergrund.xls')
